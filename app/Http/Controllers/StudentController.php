<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Student;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;


class StudentController extends Controller
{
    // public function index()
    // {
    //     return view('');
    // }

    public function create(Request $req)
    {
        $student = new Student;
        $student->name = $req->name;
        $student->rollno = $req->rollno;
        $student->email = $req->email;
        $student->phoneno = $req->phoneno;
        $student->dob = $req->dob;
        $student->course = $req->course;
        // $student->profile_img = $req->profile_img;

        if ($req->hasFile('profile_img')) {
            $file = $req->file('profile_img');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('uploads/students/', $filename);
            $student->profile_img = $filename;
        }



        $req->validate([
            'name' => 'required|regex:/^[\p{L}\s-]+$/|max:255',
            'rollno' => 'required',
            'email' => 'required|email',
            'phoneno' => 'required|digits:10',
            'dob' => 'required',
            'course' => 'required|string',
            // 'profile_img' => 'required|mimetypes:image/jpeg,image/png,image/bmp,image/svg',


        ]);

        $student->save();
        return redirect('list');
    }


    public function store(Request $req)
    {
        //
    }


    public function show(Request $req)
    {
        $data = Student::all();
        return view('list', ['students' => $data]);
    }

    public function showData($id)    //show data for update
    {
        $data = Student::find($id);
        return view('update', ['data' => $data]);
    }


    public function update(Request $req)
    {
        $data = Student::find($req->id);
        $data->name = $req->name;
        $data->rollno = $req->rollno;
        $data->email = $req->email;
        $data->phoneno = $req->phoneno;
        $data->dob = $req->dob;
        $data->course = $req->course;

        if ($req->hasFile('profile_img')) {
            $destination = 'uploads/students/' . $data->profile_img;
            if (File::exists($destination)) {
                File::delete($destination);
            }
            $file = $req->file('profile_img');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('uploads/students/', $filename);
            $data->profile_img = $filename;
        }

        $req->validate([
            'name' => 'required|regex:/^[\p{L}\s-]+$/|max:255',
            'rollno' => 'required',
            'email' => 'required',
            'phoneno' => 'required|digits:10',
            'dob' => 'required',
            'course' => 'required|string'

        ]);

        $data->save();
        return redirect('list');
    }

    public function destroy($id)
    {
        Student::find($id)->delete();
        return redirect('list');
    }

    function login()
    {
        return view('auth/login');
    }

    function register()
    {
        return view('auth/register');
    }

    function save(Request $req)
    {
        // validate requests
        $req->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:student',
            'password' => 'required|string|min:8|confirmed',

        ]);
    }

    public function status_update($id)
    {
        //get student status with the help of studentID
        $student = DB::table('students')
            ->select('status')
            ->where('id', '=', $id)
            ->first();

        //chech the student status
        if ($student->status == '1') {
            $status = '0';
        } else {
            $status = '1';
        }

        //update student status
        $values = array('status' => $status);
        DB::table('students')->where('id', $id)->update(($values));

        session()->flash('msg', 'Student status has been updated successfully!!');
        return redirect('list');
    }
}

@extends('layouts.app')

@section('content')

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


<h1 class="ml-3 text-uppercase text-center">Update</h1>

<div class="jumbotron">
    <form action="/update" method="POST" enctype="multipart/form-data" class="col-md-4 d-flex flex-column justify-content-center">
        @csrf



        <input type="hidden" name="id" value={{$data['id']}}>
        <div class="form-group">
            <label for="name">NAME</label>
            <input type="text" class="form-control" name="name" value="{{$data['name']}}">
            <span class="text-danger">@error('name'){{$message}}@enderror</span>


        </div>
        <div class="form-group">
            <label for="rollno">ROLLNO</label>
            <input type="text" class="form-control" name="rollno" value="{{$data['rollno']}}">
            <span class="text-danger">@error('rollno'){{$message}}@enderror</span>

        </div>
        <div class="form-group">
            <label for="address">EMAIL</label>
            <input type="text" class="form-control" name="email" value="{{$data['email']}}">
            <span class="text-danger">@error('email'){{$message}}@enderror</span>

        </div>
        <div class="form-group">
            <label for="phoneno">PHONE NO</label>
            <input type="tel" class="form-control" name="phoneno" value="{{$data['phoneno']}}">
            <span class="text-danger">@error('phoneno'){{$message}}@enderror</span>

        </div>
        <div class="form-group">
            <label for="dob">DOB</label>
            <input type="date" class="form-control" name="dob" value="{{$data['dob']}}">
            <span class="text-danger">@error('dob'){{$message}}@enderror</span>

        </div>
        <div class="form-group">
            <label for="course">COURSE</label>
            <input type="text" class="form-control" name="course" value="{{$data['course']}}">
            <span class="text-danger">@error('course'){{$message}}@enderror</span>

        </div>
        <div class="form-group">
            <label for="profile_img">Upload Image</label>
            <input type="file" class="form-control" name="profile_img">
            <img src="{{ asset('uploads/students/'. $data['profile_img']) }}" width="70px" height="70px" alt="image">



        </div>

        <button type="submit" class="btn btn-primary">Update</button>
    </form>
</div>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
@endsection

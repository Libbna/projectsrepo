<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">



@extends('layouts.app')

@section('content')


<h1 class="ml-3 text-uppercase text-center">Create Student</h1>

<div class="jumbotron">
    <form action="{{ url('/addStud') }}" method="POST" enctype="multipart/form-data" class="col-md-4 d-flex flex-column justify-content-center">
        @csrf
        <div class="form-group ">
            <label for="name">NAME</label>
            <input type="text" class="form-control" name="name">
            <span class="text-danger">@error('name'){{$message}}@enderror</span>
        </div>
        <div class="form-group">
            <label for="rollno">ROLL NO</label>
            <input type="text" class="form-control" name="rollno">
            <span class="text-danger">@error('rollno'){{$message}}@enderror</span>
        </div>
        <div class="form-group">
            <label for="address">Email </label>
            <input type="email" class="form-control" name="email">
            <span class="text-danger">@error('email'){{$message}}@enderror</span>
        </div>
        <div class="form-group">
            <label for="phone">PHONE NO</label>
            <input type="tel" class="form-control" name="phoneno">
            <span class="text-danger">@error('phoneno'){{$message}}@enderror</span>
        </div>
        <div class="form-group">
            <label for="dob">DOB</label>
            <input type="date" class="form-control" name="dob">
            <span class="text-danger">@error('dob'){{$message}}@enderror</span>
        </div>
        <div class="form-group">
            <label for="dob">COURSE</label>
            <input type="text" class="form-control" name="course">
            <span class="text-danger">@error('course'){{$message}}@enderror</span>
        </div>
        <div class="form-group mb-2">
            <input type="file" class="form-control" name="profile_img">
            <span class="text-danger">@error('profile_img'){{$message}}@enderror</span>
        </div>
        <div>
            <button type="submit" class="btn btn-primary text-uppercase">create</button>
            <a href="/list" class="btn btn-secondary text-uppercase">view list</a>
        </div>

    </form>

</div>




<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div> -->


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
@endsection

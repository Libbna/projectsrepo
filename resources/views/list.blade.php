<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
</head>

@extends('layouts.app')

@section('content')




<h1 class="text-center">Student List</h1>

<table class="table table-hover table-dark">
    <thead>
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Roll No.</th>
            <th scope="col">Email</th>
            <th scope="col">Phone no</th>
            <th scope="col">DOB</th>
            <th scope="col">Course</th>
            <th scope="col">Image</th>
            <th scope="col">Status</th>
        </tr>
    </thead>
    @foreach ($students as $student)
    <tbody>
        <tr>
            <th scope="row">{{$student['name']}}</th>
            <td>{{$student['rollno']}}</td>
            <td>{{$student['email']}}</td>
            <td>{{$student['phoneno']}}</td>
            <td>{{$student['dob']}}</td>
            <td>{{$student['course']}}</td>
            <td>
                <img src="{{ asset('uploads/students/'. $student['profile_img']) }}" width="70px" height="70px" alt="image">
            </td>
            <td>
                <?php if ($student->status == '1') { ?>
                    <a href="{{url('/status_update', $student->id)}}" class="btn btn-success">Active</a>
                <?php } else { ?>
                    <a href="{{url('/status_update', $student->id)}}" class="btn btn-danger">Inactive</a>
                <?php  }  ?>

            </td>
            <td>
                <a href={{"update/".$student['id']}}>Update</a>
                <span>/</span>
                <a href={{"destroy/".$student['id']}}>Delete</a>
            </td>
        </tr>


    </tbody>
    @endforeach
</table>

<div class="text-center">
    <a href="/home" class="btn btn-primary text-uppercase align-self-center">Create</a>

</div>

<script>
    $(function() {
        $('#toggle-two').bootstrapToggle({
            on: 'Enabled',
            off: 'Disabled'
        });
    })
</script>



<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
@endsection

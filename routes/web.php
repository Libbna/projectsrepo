

<?php

use App\Http\Controllers\StudentController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::view('addStud', 'addStud');
Route::post('/addStud', [StudentController::class, 'create']);

// Route::post('/', [StudentController::class, 'store']);
Route::get('list', [StudentController::class, 'show'])->middleware('auth'); //store

Route::get('destroy/{id}', [StudentController::class, 'destroy']);

Route::get('update/{id}', [StudentController::class, 'showData'])->middleware('auth');
Route::post('update', [StudentController::class, 'update']);

Route::get('/status_update/{id}', [StudentController::class, 'status_update'])->name('changeStatus');

// Route::view('register', 'register');
// Route::view('login', 'login');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
